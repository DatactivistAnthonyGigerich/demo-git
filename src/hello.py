def calculate_sum_of_numbers_list(numbers_list):
    sum = 0
    for number in numbers_list:
        sum += number
    return sum


sum = calculate_sum_of_numbers_list([1, 2, 3, 4, 5])
print(sum)
